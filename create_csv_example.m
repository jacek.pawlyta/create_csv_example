% Matlab 2011a script which demonstates how to
% create CSV file with header lines
%
% author: Jacek Pawlyta
% date: 2019-11-07
% version: 1.0
% licence:  GPL 2.0

% define file name to which will will write the data
fileName = 'datafile.csv';

% write some prompt
disp(' ');
disp(['Let''s try to write following table into CSV file: ',fileName,', smile']);

% build two lines header of the table
header = {'temperatura','ciśnienie','wilgotność względna'};
headerUnits = {'°C', 'hPa', '%'};

% just put some near-real data in the matrix
values = [2,988,67;3,989,72;3.5,988.5,71];

% show on the screen how the data looks like
disp(' ');
disp(header);
disp(headerUnits);
disp(values);


% open datafile to write headers in it
% if file existed before it will be overwritten 'w+'
dataFileId = fopen(fileName,'w+');

% write down the first line of the header into the datafile
% make CSV file semicollon separated
fprintf(dataFileId,'%s%s%s%s%s%s%s\n', '"',header{1},'";"',header{2},'";"',header{3},'"');

% write down the second line of header into datafile (units)
fprintf(dataFileId,'%s%s%s%s%s%s%s\n', '"',headerUnits{1},'";"',headerUnits{2},'";"',headerUnits{3},'"');

% close the datafile so we can append to it some data in other way
fclose(dataFileId);

% write some data to the datafile
dlmwrite(fileName,values, '-append','delimiter', ';');
%write some more data to the file (the same values as prevoiusly)
dlmwrite(fileName,values, '-append','delimiter', ';');

% say goodbay
disp(['Try to import data from CSV file: ',fileName,' to Libre Calc or Excel, keep smiling']);

